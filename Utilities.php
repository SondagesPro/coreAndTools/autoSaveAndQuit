<?php
/**
 * Some Utilities for this pluginn and other plugins
 * 
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace autoSaveAndQuit;

use App;
use Yii;
class Utilities
{
    /* @var float API The api version number */
    CONST API = 1.2;

    /* @var array tyhe default settings, see autoSaveAndQuit->settings */
    CONST DefaultSettings = array(
        'autoSaveTimeOut' => "",
        'autoSaveTimeOutFor' => 'reloaded',
        'autoSaveTimeAlert' => "",
        'autoSaveAndQuitActive' => 'autosave',
        'autoSaveAndQuitRestrict' => "available",
        'autoSaveAndQuitResetSubmitdate' => 'auto'
    );

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSetting($surveyId, $sSetting) {
        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'autoSaveAndQuit')
        );
        if(!$oPlugin || !$oPlugin->active) {
            return null;
        }
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND '.App()->getDb()->quoteColumnName('key').' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if(!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if($value !== '') {
                return $value;
            }
        }

        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND '.App()->getDb()->quoteColumnName('key').' = :key AND model IS NULL',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
            )
        );
        if(!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if($value !== '') {
                return $value;
            }
        }
        if (isset(self::DefaultSettings[$sSetting])) {
            return self::DefaultSettings[$sSetting];
        }
        return null;
    }

    /**
     * Check if autosave and quit is disable for current survey and response
     * @param integer $surveyid (must exist)
     * @todo : move to Utilities / helper
     * @return boolean
     */
    public static function isSaveAndQuit($surveyId)
    {
        $autoSaveAndQuitActive = self::getSetting($surveyId, 'autoSaveAndQuitActive');
        if(App()->getRequest()->getPost('autosaveandquit')) {
            return true;
        }
        switch($autoSaveAndQuitActive) {
            case 'never':
                return false;
            case 'always':
                return true;
            case 'autosave':
            default:
                return App()->getRequest()->getPost("saveandquit-autosave");
        }
    }

    /**
     * Check if autosave and quit is disable for current survey and response
     * @param integer $surveyId
     * @return boolean
     */
    public static function isDisableSaveAndQuit($surveyId)
    {
        $autoSaveAndQuitRestrict = self::getSetting($surveyId, 'autoSaveAndQuitRestrict');
        switch($autoSaveAndQuitRestrict) {
            case "none":
                // return false
                break;
            case "ondemand":
                if(empty(App()->getRequest()->getPost('autosaveandquit'))) {
                    return true;
                }
                break;
            case "reloaded":
                if(!Yii::getPathOfAlias('reloadAnyResponse')) {
                    return true;
                }
                if(\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) != \reloadAnyResponse\Utilities::getCurrentSrid($surveyId)) {
                    return true;
                }
                break;
            case "available":
            default:
                $oSurvey = \Survey::model()->findByPk($surveyId);
                $tokenEnableSave = !$oSurvey->isAnonymized && $oSurvey->isTokenAnswersPersistence && $oSurvey->hasTokensTable;
                $alreadySaved = !empty($_SESSION['survey_'.$surveyId]['scid']);
                if($tokenEnableSave || $alreadySaved) {
                    return false;
                }
                $canBeReloaded = false;
                $alreadyReloaded = false;
                if(Yii::getPathOfAlias('reloadAnyResponse')) {
                    $alreadyReloaded = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) != \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
                    $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
                    $StartUrl = new \reloadAnyResponse\StartUrl($surveyId, $token);
                    $canBeReloaded = $StartUrl->getUrl(\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId));
                }
                if(!$tokenEnableSave && !$alreadySaved && !$alreadyReloaded && !$canBeReloaded) {
                    return true;
                }
                break;
        }
        return false;
    }
    
}
