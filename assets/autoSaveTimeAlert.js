/**
 * Javascript for reload any response
 * @author Denis Chenu <https://sondages.pro
 * @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD 3 Clause
 */

var autoSaveTimeOut = {
    surveyTimeOut : 0,
    surveyTimeOutAlert : 0,
    idleTime : 0,
    init : function (options) {
        if ($("button[value='movenext'],button[value='movesubmit'],button[name='saveall'][value='saveall']").length == 0) {
            // No need to save without move next, submit or saveall button
            return;
        }
        this.surveyTimeOut = options.autoSaveTimeOut;
        this.surveyTimeOutAlert = options.autoSaveTimeAlert;
        var context = this;
        $(function() {
            var autoSaveTimeAlertIdleInterval = window.setInterval(function() {
                    context.timerIncrement();
                }, 60 * 1000); // One minute 60 * 1000
        });
        this.AccessTimeOptOutReset();
    },
    AccessTimeOptOutAlert: function() {
        $("#modal-saveandquit-timer").modal('show');
    },
    AccessTimeOptOut: function() {
        $("form#limesurvey").append("<input type='hidden' name='saveandquit-autosave' value='1'>");
        $("form#limesurvey").append("<button type='submit' name='saveall' value='saveall' class='hidden'>");
        $("form#limesurvey [name='saveall']").last().click();
    },
    AccessTimeOptOutReset: function() {
        var context = this;
        $(document).on('click keypress mousemove scroll',function(){
            context.idleTime = 0;
            $("[data-saveandquit-timecounter]").text(context.surveyTimeOut);
            $("#modal-saveandquit-timer").modal('hide');
        });
    },
    timerIncrement: function() {
        this.idleTime = this.idleTime + 1;
        $("[data-saveandquit-timecounter]").text( this.surveyTimeOut - this.idleTime );
        if(this.surveyTimeOutAlert && this.surveyTimeOutAlert <= this.idleTime) {
            this.AccessTimeOptOutAlert();
        }
        if(this.surveyTimeOut && this.surveyTimeOut <= this.idleTime) {
            this.AccessTimeOptOut();
        }
    }
}
