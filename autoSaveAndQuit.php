<?php
/**
 * Allow auto save and quit, usage with reloadAnyResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2022 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 1.5.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class autoSaveAndQuit extends PluginBase {

    protected $storage = 'DbStorage';

    static protected $description = 'Allow to use an autosaving mode in survey. To be used with reloadAnyResponse.';
    static protected $name = 'autoSaveAndQuit';

    /* @var integer:null : keep track of current surveyid between events */
    private $surveyId = null;

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => 'The default settings for all surveys.',
        ),
        'autoSaveTimeOut'=>array(
            'type'=>'int',
            'label' => 'Auto save and close current responses (with a javascript solution) in minutes.',
            'help' => 'If user didn‘t do any action on his browser during access time, save and show information. Set to an empty disable this feature.',
            'htmlOptions'=>array(
                'min'=>1,
                'placeholder'=>'Disable',
            ),
            'default' => "",
        ),
        'autoSaveTimeOutFor'=>array(
            'type'=>'select',
            'label' => 'Auto save and close current responses for',
            'help' => 'Save and quit automatically only when response have this current state.',
            'options'=>array(
                // Add more after ? 
                'reloaded' => "Only for reloaded response",
                'available' => "When reload is potentially available : reloaded response, single token response (response persistance) and already saved response.",
                'always' => "Always (even if the response can not be saved automatically)",
            ),
            'default' => 'reloaded',
        ),
        'autoSaveTimeAlert'=>array(
            'type'=>'int',
            'label' => 'Time for alert shown for optout of survey (before survey is saved)',
            'help' => 'Set to empty to disable. This alert is shown after X minutes, where X is the number here.',
            'htmlOptions'=>array(
                'min'=>1,
                'placeholder'=>'Disable',
            ),
            'default' => "",
        ),
        'autoSaveAndQuitActive'=>array(
            'type'=>'select',
            'label' => 'Quit after save',
            'options'=>array(
                'never' => "Never",
                'autosave' => "With autosave",
                'always' =>"Always",
            ),
            'default' => 'autosave',
            'help' => "On demand is always active"
        ),
        'autoSaveAndQuitRestrict'=>array(
            'type'=>'select',
            'label' => 'Restriction for quit after save',
            'options'=>array(
                // Add more after ?
                "ondemand" => "Only on demand (adding autosaveandquit boolean post when save)",
                "reloaded" => "Only for reloaded response",
                "available" => "When reload is potentially available : single token response (response persistance) and already saved response",
                "none" => "None (always quit after save)",
            ),
            'default' => "available", // Since activate by default only with autosave
        ),
        'autoSaveAndQuitResetSubmitdate'=>array(
            'type'=>'select',
            'label' => 'Reset submitdate when save reponse, this set submitdate to null (set as draft)',
            'options'=>array(
                "no" => "Never",
                "auto" => "When reponse is automatically saved only",
                "alway" => "When reponse is saved (always)",
            ),
            'default' => "auto",
        ),
    );

    /** @inheritdoc **/
    public function init()
    {
        if (App() instanceof CConsoleApplication) {
            return;
        }
        /* Check for renderMessage */
        $this->subscribe('beforeActivate');
        if (version_compare(App()->getConfig("versionnumber"),"3.10.0","<=")) {
            $this->log("autoSaveAndQuit need LimeSurvey 3.10.0 and up version",'error');
            return;
        }

        /* Check if saved */
        $this->subscribe('beforeSurveyPage');
        /* Survey settings */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        /* Offering an event, need inform API version */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
    }

    public function beforeActivate() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (version_compare(App()->getConfig("versionnumber"),"3.10.0","<=")) {
            $this->getEvent()->set('message', $this->gT("You need LimeSurvey 3.10 or up version"));
            $this->getEvent()->set('success', false);
        }
        if(!Yii::getPathOfAlias('reloadAnyResponse')) {
            $this->getEvent()->set('message', $this->gT("You need reloadAnyResponse plugin"));
            $this->getEvent()->set('success', false);
        }
    }
    /** @inheritdoc **/
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $surveyId = $oEvent->get('survey');
        if(!Yii::getPathOfAlias('reloadAnyResponse')) {
            $oEvent->set("surveysettings.{$this->id}", array(
                'name' => get_class($this),
                'settings' => array(
                    'autoSaveAvailable' => array(
                        'type' => "info",
                        "content" => $this->gT("You need reloadAnyResponse Plugin activated"),
                        'class' => array("alert alert-danger"),
                    )
                ),
            ));
            return;
        }
        /* currentDefault translation */
        $autoSaveTimeOutDefault = $this->get('autoSaveTimeOut',null,null,$this->settings['autoSaveTimeOut']['default']) ? $this->get('autoSaveTimeOut',null,null,$this->settings['autoSaveTimeOut']['default']) : gT('Disable');
        $autoSaveTimeAlertDefault = $this->get('autoSaveTimeAlert',null,null,$this->settings['autoSaveTimeAlert']['default']) ? $this->get('autoSaveTimeAlert',null,null,$this->settings['autoSaveTimeAlert']['default']) : gT('Disable');
        $autoSaveAndQuitActiveDefault = $this->get('autoSaveAndQuitActive',null,null,$this->settings['autoSaveAndQuitActive']['default']);
        switch($autoSaveAndQuitActiveDefault) {
            case "never":
                $autoSaveAndQuitActiveDefault = $this->gT("Never");
                break;
            case 'autosave':
                $autoSaveAndQuitActiveDefault = $this->gT("With autosave");
                break;
            case 'always':
                $autoSaveAndQuitActiveDefault = $this->gT("Always");
                break;
            default:
                // Unknow …
        }
        $autoSaveAndQuitRestrictDefault = $this->get('autoSaveAndQuitRestrict',null,null,$this->settings['autoSaveAndQuitRestrict']['default']);
        switch($autoSaveAndQuitRestrictDefault) {
            case "ondemand":
                $autoSaveAndQuitRestrictDefault = $this->gT("Only on demand (with autosaveandquit param).");
                break;
            case "reloaded":
                $autoSaveAndQuitRestrictDefault = $this->gT("Only for reloaded response");
                break;
            case "available":
                $autoSaveAndQuitRestrictDefault = $this->gT("For reloaded response, single token response and already saved response");
                break;
            case "none":
                $autoSaveAndQuitRestrictDefault = $this->gT("None (always quit after save)");
                break;
            default:
                // Unknow …
        }
        $autoSaveTimeOutForActiveDefault = $this->get('autoSaveTimeOutFor',null,null,$this->settings['autoSaveTimeOutFor']['default']);
        switch($autoSaveTimeOutForActiveDefault) {
            case 'reloaded':
                $autoSaveTimeOutForActiveDefault = $this->gT("For reloaded response");
                break;
            case 'available':
                $autoSaveTimeOutForActiveDefault = $this->gT("For reloaded response, single token response and already saved response");
                break;
            case 'always':
                $autoSaveTimeOutForActiveDefault = $this->gT("Always (even to show save form)");
                break;
            default:
                // Unknow …
        }
        $autoSaveAndQuitResetSubmitdateDefault = $this->get('autoSaveAndQuitResetSubmitdate',null,null,$this->settings['autoSaveAndQuitResetSubmitdate']['default']);
        switch($autoSaveAndQuitResetSubmitdateDefault) {
            case "no":
                $autoSaveAndQuitResetSubmitdateDefault = $this->gT("Never");
                break;
            case "auto":
                $autoSaveAndQuitResetSubmitdateDefault = $this->gT("When reponse is automatically saved only");
                break;
            case "alway":
                $autoSaveAndQuitResetSubmitdateDefault = $this->gT("When reponse is saved (always)");
                break;
            default:
                // Unknow …
        }
        $autoSaveTimeOutHelp = "<div>".$this->gT("Use 0 to disable. This show a static page to user after save.")."</div>";
        if(Survey::model()->findByPk($surveyId)->allowsave != 'Y') {
            $autoSaveTimeOutHelp .= "<div class='text-danger'>".$this->gT("Your survey settings disallow save, auto save system didn't test is save is allowed. You can use it only for reloaded response.")."</div>";
        }
        $allowSave = Survey::model()->findByPk($surveyId)->allowsave != "Y";
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'autoSaveAvailable' => array(
                    'type'=>'info',
                    'content'=> $this->gT("This need to allow participants to save and resume later in survey notification settings"),
                    'class' => array(
                        $allowSave ? 'alert alert-danger' : 'hidden'
                    )
                ),
                'autoSaveTimeOut'=>array(
                    'type'=>'int',
                    'label'=> $this->gT("Auto save and close current responses (with a javascript solution) in minutes."),
                    'help' => $autoSaveTimeOutHelp,
                    'htmlOptions'=>array(
                        'min'=>0,
                        'placeholder' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveTimeOutDefault)),
                    ),
                    'current'=>$this->get('autoSaveTimeOut','Survey',$surveyId,"")
                ),
                'autoSaveTimeOutFor'=>array(
                    'type'=>'select',
                    'label' => $this->gT('Needed response state to auto saving response'),
                    'help' => $this->gT('Save automatically only when response have this current state. This manage the save action in javascript only.'),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveTimeOutForActiveDefault)),
                    ),
                    'options'=>array(
                        // Add more after ? 
                        'reloaded' => $this->gT("Only for reloaded response"),
                        'available' => $this->gT("For reloaded response, single token response (response persistance) and already saved response"),
                        'always' => $this->gT("Always"),
                    ),
                    'current'=>$this->get('autoSaveTimeOutFor','Survey',$surveyId,"")
                ),
                'autoSaveTimeAlert'=>array(
                    'type'=>'int',
                    'label'=>$this->gT("Time for alert shown for optout of survey (before survey is saved)"),
                    'help' => $this->gT("Use 0 to disable."),
                    'htmlOptions'=>array(
                        'min'=>0,
                        'placeholder' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveTimeAlertDefault)),
                    ),
                    'current'=>$this->get('autoSaveTimeAlert','Survey',$surveyId,"")
                ),
                'autoSaveAndQuitActive'=>array(
                    'type'=>'select',
                    'label' => $this->gT("Close survey after save"),
                    'help' => $this->gT("If enable : return to information page."),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveAndQuitActiveDefault)),
                    ),
                    'options'=>array(
                        'never' => $this->gT("Never (except on demand)"),
                        'autosave' => $this->gT("With autosave (and on demand)"),
                        'always' => $this->gT("Always"),
                    ),
                    'current'=>$this->get('autoSaveAndQuitActive','Survey',$surveyId,"")
                ),
                'autoSaveAndQuitRestrict'=>array(
                    'type'=>'select',
                    'label' => $this->gT('Restriction for quit after save'),
                    'help' => $this->gT('Even with always, you can allow to return to survey in specific situation.'),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveAndQuitRestrictDefault)),
                    ),
                    'options'=>array(
                        // Add more after ?
                        "ondemand" => $this->gT("Only on demand (adding autosaveandquit boolean post when save)"),
                        "reloaded" => $this->gT("Only for reloaded response"),
                        "available" => $this->gT("When reload is potentially available : single token response (response persistance) and already saved response"),
                        "none" => $this->gT("None (always if enable)"),
                    ),
                    'current'=>$this->get('autoSaveAndQuitActive','Survey',$surveyId,"")
                ),
                'autoSaveAndQuitResetSubmitdate'=>array(
                    'type'=>'select',
                    'label' => $this->gT("Reset submitdate when save reponse"),
                    'help' => $this->gT("This set the survey at not submitted. This setting don't replace reloadAnyResponse setting, and it used only when quit after save."),
                    'htmlOptions' => array(
                        'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"),$autoSaveAndQuitResetSubmitdateDefault)),
                    ),
                    'options'=>array(
                        "no" => $this->gT("Never"),
                        "auto" => $this->gT("When reponse is automatically saved only"),
                        "alway" => $this->gT("When reponse is saved (always)"),
                    ),
                    'current'=>$this->get('autoSaveAndQuitResetSubmitdate','Survey',$surveyId,"")
                ),
            ),
        ));
    }

    /* @see plugin event */
    public function addPluginTwigPath() 
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__)."/views";
        $this->getEvent()->append('add', array($viewPath));
    }

    /* @see plugin event
     * Doing this when twig is loaded first : sure to have $_SESSION totally set
     **/
    public function checkAccessTimeOutScriptByTwig() 
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        if(empty($oSurvey)) {
            return;
        }
        if(!$this->isAutoSaveTimeOut($this->surveyId)) {
            return;
        }
        if(!empty($_SESSION['survey_'.$this->surveyId]['srid']) || $oSurvey->format == "A") {
            $this->addAccessTimeOutScript($this->surveyId);
        }
    }

    /** @see event */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if(!Yii::getPathOfAlias('reloadAnyResponse')) {
            $this->log("autoSaveAndQuit need reloadAnyResponse plugin",'error');
            return;
        }
        $surveyid = $this->getEvent()->get('surveyId');
        if (Survey::model()->findByPk($surveyid)->allowsave != "Y") {
            return;
        }
        $this->surveyId = $surveyid;
        $this->subscribe('beforeTwigRenderTemplate','checkAccessTimeOutScriptByTwig');
        $this->checkSaveAndQuit($surveyid);
    }

    /** @inheritdoc **/
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /**
     * Add the javascript solution for save survey
     * @param $surveyId
     **/
    public function addAccessTimeOutScript($surveyId)
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $autoSaveTimeOut = $this->getCurrentSurveySetting('autoSaveTimeOut',$surveyId);
        $autoSaveTimeAlert = $this->getCurrentSurveySetting('autoSaveTimeAlert',$surveyId);
        if(empty($autoSaveTimeOut)) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $autoSaveAndQuitdata = array(
            'lang'=> array(
                "Warning!" => $this->gT("Warning!"),
                "This window will close in %s minute(s)." => $this->gT("This window will close in %s minute(s)."),
                "This window will close in %s minute(s). Current response will be saved as uncomplete." => $this->gT("This window will close in %s minute(s). Current response will be saved as uncomplete."),
                "Current response will be saved as uncomplete." => $this->gT("Current response will be saved as uncomplete."),
            ),
            'quit' => $this->getCurrentSurveySetting('autoSaveAndQuitActive',$surveyId) != 'never',
            'timecounter' => "<span data-saveandquit-timecounter=1></span>"
        );
        
        $aData = array(
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
            'autoSaveAndQuit' => $autoSaveAndQuitdata
        );
        Template::getInstance(null,$surveyId);
        $this->subscribe('getPluginTwigPath','addPluginTwigPath');
        $surveyTimeOutAlert = App()->twigRenderer->renderPartial('/subviews/messages/saveandquit_surveyTimeOutAlert.twig', $aData);
        $this->unsubscribe('getPluginTwigPath');
        $modalScript = "$('body').prepend(".json_encode($surveyTimeOutAlert).");\n";
        App()->getClientScript()->registerScript("autoSaveTimeOutModal",$modalScript,LSYii_ClientScript::POS_READY);
        $jsonOption = array(
            'autoSaveTimeOut' => $autoSaveTimeOut,
            'autoSaveTimeAlert' => ($autoSaveTimeOut - $autoSaveTimeAlert) > 0 ? ($autoSaveTimeOut - $autoSaveTimeAlert) : 0,
        );
        App()->getClientScript()->registerScriptFile(App()->assetManager->publish(dirname(__FILE__) . '/assets/autoSaveTimeAlert.js'));
        App()->getClientScript()->registerScript("autoSaveTimeOutInit","window.autoSaveTimeOut.init(".json_encode($jsonOption).")\n;",LSYii_ClientScript::POS_READY);/* Check with ajax mode …*/
    }

    /**
     * Check id save and quit is active
     * Register to beforeTwigRenderTemplate if yes
     * @return boolean
     */
    private function checkSaveAndQuit($surveyid)
    {
        if(!App()->getRequest()->getParam('saveall')) {
            return false;
        }
        $oSurvey = Survey::model()->findByPk($surveyid);
        if(empty($oSurvey) || $oSurvey->active != 'Y') {
            return false;
        }
        
        $currentSrid = isset($_SESSION['survey_'.$surveyid]['srid']) ? $_SESSION['survey_'.$surveyid]['srid'] : null;
        if(!$currentSrid) {
            return false;
        }
        if(!\autoSaveAndQuit\Utilities::isSaveAndQuit($surveyid)) {
            return false;
        }
        if(\autoSaveAndQuit\Utilities::isDisableSaveAndQuit($surveyid)) {
            return false;
        }
        $this->unsubscribe('beforeTwigRenderTemplate');
        $this->surveyId = $surveyid;
        $this->subscribe('beforeTwigRenderTemplate','SaveAndQuit');
        return true;
    }

    /**
     * Save and quit at the 1st time displaying save page
     * See getPluginTwigPath event
     * @return void
     */
    public function SaveAndQuit()
    {
        $surveyId = $this->surveyId;
        if(empty($surveyId)) {
            throw new CHttpException(500);
            return;
        }
        $currentSrid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId); 
        if(empty($currentSrid)) {
            // throw new CHttpException(500);
            return;
        }
        $step = isset($_SESSION['survey_'.$surveyId]['step']) ? $_SESSION['survey_'.$surveyId]['step'] : 0;
        $_SESSION['survey_'.$surveyId]['prevstep'] = "saveall";
        LimeExpressionManager::JumpTo($step, false);
        $oResponse = SurveyDynamic::model($surveyId)->findByPk($currentSrid);
        if(empty($oResponse)) {
            /* Throw error */
            return;
        }
        /* OK, we save and quit now */
        $this->unsubscribe('beforeTwigRenderTemplate');
        $oResponse->lastpage = $step; // Or restart at 1st page ?

        $autoSaveAndQuitResetSubmitdate = $this->getCurrentSurveySetting('autoSaveAndQuitResetSubmitdate',$surveyId);
        if($autoSaveAndQuitResetSubmitdate == 'alway') {
            $oResponse->submitdate = null;
        }
        $isAutoSave = (bool)Yii::app()->getRequest()->getPost("saveandquit-autosave"); // @todo : review
        if($autoSaveAndQuitResetSubmitdate == 'auto' && $isAutoSave) {
            $oResponse->submitdate = null;
        }
        $oResponse->save();
        \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid'=>$surveyId,'srid'=>$currentSrid));
        $restartUrl = $startUrl = App()->getController()->createUrl("survey/index",array(
            'sid' => $surveyId,
            'newtest' => "Y",
        ));
        if(\reloadAnyResponse\Utilities::getCurrentToken($surveyId)) {
            $restartUrl = $startUrl = App()->getController()->createUrl("survey/index",array(
                'sid' => $surveyId,
                'token' => \reloadAnyResponse\Utilities::getCurrentToken($surveyId),
                'newtest' => "Y",
            ));
        }
        $oStartUrl = new \reloadAnyResponse\StartUrl(
            $surveyId,
            \reloadAnyResponse\Utilities::getCurrentToken($surveyId)
        );
        $reloadUrl = $oStartUrl->getUrl($currentSrid);
        /* Use twig renderer */
        $oSurvey = Survey::model()->findByPk($surveyId);
        $language = App()->getLanguage();
        if(!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }

        $aRenderData = array();
        $renderData['aSurveyInfo'] = getSurveyInfo($surveyId,$language);
        $renderData['aSurveyInfo']['include_content'] = 'saveandquit_savedMessage';
        $renderData['aSurveyInfo']['showprogress'] = false;
        $renderData['autoSaveAndQuit'] = array(
            'lang' => array(
                "Your responses were saved successfully, you can close this window." => $this->gT("Your responses were saved successfully, you can close this window."),
                "Restart this survey" => $this->gT("Restart this survey"),
                "Reload this response" => $this->gT("Reload this response"),
            ),
            'restartUrl' => $restartUrl,
            'reloadUrl' => $reloadUrl,
            'reloaded' => \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId)
        );
        App()->getClientScript()->registerScript("autoSaveAndQuitQuit",'$(document).trigger("saveandquit:quit");;',LSYii_ClientScript::POS_END);
        Template::model()->getInstance(null, $surveyId);
        $this->subscribe('getPluginTwigPath','addPluginTwigPath');
        killSurveySession($surveyId);
        resetQuestionTimers($surveyId);
        App()->twigRenderer->renderTemplateFromFile('layout_global.twig', $renderData, false);
    }

    /**
     * Check if js autosave is enable for current survey and response
     * @param \Survey $oSurvey
     * @return boolean
     */
    private function isAutoSaveTimeOut($surveyId)
    {
        $autoSaveTimeOutFor = $this->getCurrentSurveySetting('autoSaveTimeOutFor',$surveyId);
        /* reloaded, available, always */
        if ($autoSaveTimeOutFor == 'always') {
            return true;
        }
        if ($autoSaveTimeOutFor == 'reloaded') {
            if(Yii::getPathOfAlias('reloadAnyResponse')) {
                if(\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) == \reloadAnyResponse\Utilities::getCurrentSrid($surveyId)) {
                    return true;
                }
            }
            return false;
        }
        if ($autoSaveTimeOutFor == 'available') {
            $oSurvey = Survey::model()->findByPk($surveyId);
            $tokenEnableSave = !$oSurvey->isAnonymized && $oSurvey->isTokenAnswersPersistence && $oSurvey->hasTokensTable;
            $alreadySaved = !empty($_SESSION['survey_'.$surveyId]['scid']);
            if($tokenEnableSave || !$alreadySaved ) {
                return true;
            }
            if(Yii::getPathOfAlias('reloadAnyResponse')) {
                if(\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) == \reloadAnyResponse\Utilities::getCurrentSrid($surveyId)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * Get current setting for current survey (use empty string as null value)
     * @param string setting to get
     * @param integer survey id
     * @return string|array|null
     */
    private function getCurrentSurveySetting($setting,$surveyId = null)
    {
        if($surveyId) {
            $value = $this->get($setting,'Survey',$surveyId,'');
            if($value !== '') {
                return $value;
            }
        }
        $default = (isset($this->settings[$setting]['default'])) ? $this->settings[$setting]['default'] : null;
        return $this->get($setting,null,null,$default);
    }

    /**
    * @inheritdoc
    * Usage of unescaped by default
    */
    public function gT($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($sToTranslate,$sEscapeMode,$sLanguage);
    }

}
