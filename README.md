# autoSaveAndQuit

Allow to use an autosaving mode in survey. 

## Usage

After activation, set the default setings for all surveys in pluigoin nsettings.

Update needed settings in plugin settings for each survey.

## Home page, copyright and support
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2019-2022 Denis Chenu <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Professional support](https://support.sondages.pro/)
